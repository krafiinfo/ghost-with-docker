#!/bin/bash

# Set the base file name
BASE_NAME="myghost"
BACKUP_FILE="$(date +%Y-%m-%d_%H:%M:%S)"

# Create the output directory if it doesn't exist
mkdir -p out

# Set the starting number for the suffix
NUM=0

# some color
c0="${reset}${bold}${cyan}"     
c1="${reset}${cyan}"   

# Loop until a file name is found that doesn't exist
while [[ -e "${BASE_NAME}${NUM}.tar.enc" ]]; do
  ((NUM++))
done


# Create the encrypted file using the new file name
sudo docker export efef1f15b461 | openssl enc -aes-256-cbc -pbkdf2 -pass pass:$1 -out out/"${BASE_NAME}_${NUM}__${BACKUP_FILE}.tar.enc" -v


# Print the name of the backup file
echo "${BASE_NAME}_${NUM}__${BACKUP_FILE}.tar.enc" 

# Exit the script with a success status
echo "${c0} Congratulation! This file was successfully produced. "
echo "${c1} Keep following. www.krafi.info www.krafi.xyz"

exit 0

